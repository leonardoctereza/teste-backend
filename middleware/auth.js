const models = require('../models');

const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { User } = require('../models');

validateToken = (req, res, next) => {
  try {
    const [, token] = req.headers.authorization.split(' ');
    const decoded = jwt.verify(token, process.env.SECRET);
    if (!decoded.active) {
      return res.status(400).json({
        message: 'User does not exist',
      });
    }
    req.user = decoded;

    next();
  } catch (error) {
    return res.status(401).json({
      message: 'Auth failed',
    });
  }
};

validateIsAdmin = (req, res, next) => {
  try {
    const [, token] = req.headers.authorization.split(' ');
    const decoded = jwt.verify(token, process.env.SECRET);
    if (decoded.role !== 'admin') {
      return res.status(403).json({
        message: 'User with insuficient permission',
      });
    }
    req.user = decoded;

    next();
  } catch (error) {
    return res.status(401).json({
      message: 'Auth failed',
    });
  }
};

validateDuplicatedUserEmail = (req, res, next) => {
  User.findOne({
    email: req.body.email,
  }).then((user) => {
    if (user) {
      return res.status(400).send({
        message: 'Email already in use',
      });
    }
    next();
  });
};

module.exports = {
  validateToken,
  validateDuplicatedUserEmail,
  validateIsAdmin,
};
