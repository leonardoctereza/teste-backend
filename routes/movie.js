const express = require('express');
const authMiddleware = require('../middleware/auth');
const MovieController = require('../controllers/movie');
const router = express.Router();

router.post('/add', authMiddleware.validateIsAdmin, MovieController.add);
router.post('/vote', authMiddleware.validateToken, MovieController.vote);
router.get('/filter', MovieController.filter);
router.get('/:id', MovieController.getFilm);

module.exports = router;
