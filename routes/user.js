const express = require('express');
const authMiddleware = require('../middleware/auth');
const UserController = require('../controllers/user');
const router = express.Router();

router.post(
  '/add',
  authMiddleware.validateDuplicatedUserEmail,
  UserController.create
);
router.get('/login', UserController.login);
router.put('/edit', validateToken, UserController.edit);
router.delete('/delete', validateToken, UserController.delete);

module.exports = router;
