require('dotenv').config();

const express = require('express');
const app = express();
const mongoose = require('mongoose');
const userRoute = require('./routes/user');
const movieRoute = require('./routes/movie');

const port = process.env.PORT || 3000;
mongoose
  .connect(`${process.env.DB_URI}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })
  .then(() => console.log('Connected to Mongo'))
  .catch((err) => console.log(`Error connecting to database: ${err}`));

mongoose.Promise = global.Promise;

app.use(express.json());
app.use('/movie', movieRoute);
app.use('/user', userRoute);

app.get('/', (req, res) => {
  res.json({ message: 'API OK' });
});

app.use((req, res, next) => {
  const error = new Error('Not found');
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message,
    },
  });
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}.`);
});
