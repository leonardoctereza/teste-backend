require('dotenv').config();

const { User } = require('../models');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

generateToken = (user) => {
  return jwt.sign(
    {
      id: user.id,
      email: user.email,
      active: true,
      role: user.role,
    },
    process.env.SECRET
  );
};

exports.create = (req, res) => {
  if (!req.body.email || !req.body.password || !req.body.name) {
    return res.status(400).send({
      message: 'Missing required field',
    });
  }
  const user = new User({
    name: req.body.name,
    email: req.body.email,
    role: req.body.role,
    active: true,
    password: req.body.password,
  });
  user
    .save()
    .then((data) => {
      const userJwt = generateToken(user);

      res.send({ data, token: userJwt });
    })
    .catch((err) => {
      res.status(500).send({
        message: 'Error while creating user',
      });
    });
};

exports.login = async (req, res) => {
  try {
    if (!req.body.email || !req.body.password) {
      return res.status(400).send({
        message: 'Missing required field',
      });
    }
    const user = await User.findOne({ email: req.body.email });
    if (!user || !user.active) {
      return res.status(500).send({
        message: 'User not found',
      });
    }
    const userJwt = generateToken(user);

    res.send({ message: 'User logged', token: userJwt });
  } catch (error) {
    res.status(500).send({
      message: 'Error while creating user',
    });
  }
};

exports.edit = async (req, res) => {
  try {
    if (!req.body) {
      return res.status(400).send({
        message: 'Missing required field',
      });
    }

    await User.findOneAndUpdate(
      { email: req.user.email, active: true },
      { $set: req.body },
      { useFindAndModify: false },
      (err, doc) => {
        if (err) throw new Error();
        else if (doc && req.body.password) {
          doc.password = req.body.password;
          doc.save();
        }
      }
    ).then((data) => {
      if (!data) {
        return res.status(400).send({
          message: 'Cannot updated User or User not found',
        });
      }
      res.send({ message: 'User updated' });
    });
  } catch (error) {
    return res.status(400).send({
      message: 'Error updating user',
    });
  }
};

exports.delete = async (req, res) => {
  try {
    await User.findOneAndUpdate(
      { email: req.user.email, active: true },
      { active: false },
      { useFindAndModify: false }
    ).then((data) => {
      if (!data) {
        return res.status(400).send({
          message: 'Cannot delete User or User not found',
        });
      }
      res.send({ message: 'User deleted' });
    });
  } catch (error) {
    res.status(500).send({
      message: 'Error while deleting user',
    });
  }
};
