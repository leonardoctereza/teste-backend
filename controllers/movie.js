const { Movie } = require('../models');

exports.add = (req, res) => {
  if (
    !req.body.name ||
    !req.body.director ||
    !req.body.gender ||
    !req.body.actors
  ) {
    return res.status(400).send({
      message: 'Missing required field',
    });
  }
  const movie = new Movie({
    name: req.body.name,
    director: req.body.director,
    gender: req.body.gender,
    actors: req.body.actors,
  });
  movie
    .save()
    .then((data) => {
      res.send({ data, message: `Movie ${req.body.name} added` });
    })
    .catch((err) => {
      res.status(500).send({
        message: 'Error while adding film',
      });
    });
};

exports.vote = (req, res) => {
  try {
    if (!req.body || !req.body.movieId || req.body.vote === undefined) {
      return res.status(400).send({
        message: 'Missing required field',
      });
    }
    if (req.body.vote < 0 || req.body.vote > 4) {
      return res.status(400).send({
        message: 'Vote need to by between 0 and 4',
      });
    }

    Movie.findOne({ _id: req.body.movieId }).then((movie) => {
      if (!movie) {
        return res.status(400).send({
          message: 'Film not found',
        });
      }

      movie.votes.push(req.body.vote);
      const sum = movie.votes.reduce((a, b) => a + b, 0);
      movie.rank = sum / movie.votes.length;
      Movie.updateOne({ _id: movie._id }, movie).then(() => {
        res.send({ message: 'Vote added' });
      });
    });
  } catch (error) {
    return res.status(400).send({
      message: 'Error updating user',
    });
  }
};

exports.filter = (req, res) => {
  if (!req.body || !req.body.search) {
    return res.status(400).send({
      message: 'Missing required field',
    });
  }
  const search = req.body.search;
  Movie.find({
    $or: [
      { name: search },
      { director: search },
      { gender: search },
      { actors: search },
    ],
  }).then((movieList) => {
    if (!movieList) {
      return res.status(200).send({
        message: 'No movie found',
      });
    }
    res.status(200).send({ movieList });
  });
};

exports.getFilm = (req, res) => {
  if (!req.params.id) {
    return res.status(400).send({
      message: 'Missing film id',
    });
  }
  const id = req.params.id;
  Movie.findById({
    _id: id,
  }).then((movie) => {
    if (!movie) {
      return res.status(400).send({
        message: `No movie was found with id ${id}`,
      });
    }
    res.status(200).send({
      data: movie,
    });
  });
};
