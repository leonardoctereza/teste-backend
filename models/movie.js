const mongoose = require('mongoose');

const Movie = mongoose.model(
  'Movie',
  new mongoose.Schema({
    name: { type: String, required: true, unique: true },
    director: { type: String, required: true },
    rank: { type: Number, default: 0 },
    gender: { type: String, default: '' },
    votes: [{ type: Number, default: null }],
    actors: [{ type: String, default: null }],
  })
);

module.exports = Movie;
