const User = require('./user');
const Movie = require('./movie');

const models = {
  User,
  Movie,
};

module.exports = models;
